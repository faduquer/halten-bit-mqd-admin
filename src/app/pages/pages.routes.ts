import { RouterModule, Routes } from '@angular/router';

// Guards
import {
    VerificaTokenGuard
} from '../guards/guards.index';

const pagesRoutes: Routes = [
    {
        path: 'inicio',
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        canActivate: [ VerificaTokenGuard ]
    },
    {
        path: 'tarifas-atributos',
        loadChildren: './tarifas-atributos/tarifas-atributos.module#TarifasAtributosModule',
        canActivate: [ VerificaTokenGuard ]
    },
    {
        path: 'tarifas-plataforma',
        loadChildren: './tarifas-plataforma/tarifas-plataforma.module#TarifasPlataformaModule',
        canActivate: [ VerificaTokenGuard ]
    },
    {
        path: 'tarifas-formatos',
        loadChildren: './tarifas-formatos/tarifas-formatos.module#TarifasFormatosModule',
        canActivate: [ VerificaTokenGuard ]
    },
    { path: '', redirectTo: '/inicio', pathMatch: 'full' }
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );
