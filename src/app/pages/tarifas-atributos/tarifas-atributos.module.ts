import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Rutas
import { TARIFAS_ATRIBUTOS_ROUTES } from './tarifas-atributos.routes';

// Components
import { TarifasAtributosComponent } from './tarifas-atributos.component';

@NgModule({
    declarations: [
        TarifasAtributosComponent
    ],
    exports: [
        TarifasAtributosComponent
    ],
    imports: [
        TARIFAS_ATRIBUTOS_ROUTES,
        CommonModule,
        FormsModule,
        ReactiveFormsModule

    ]
})

export class TarifasAtributosModule { }
