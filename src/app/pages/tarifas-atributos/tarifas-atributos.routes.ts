import { RouterModule, Routes } from '@angular/router';

// Components
import { TarifasAtributosComponent } from './tarifas-atributos.component';

// Guards
import {
    VerificaTokenGuard
} from '../../guards/guards.index';

const tarifasAtributosRoutes: Routes = [
    {
        path: '',
        component: TarifasAtributosComponent,
        canActivate: [
            VerificaTokenGuard
        ]
    }
];

export const TARIFAS_ATRIBUTOS_ROUTES = RouterModule.forChild( tarifasAtributosRoutes );
