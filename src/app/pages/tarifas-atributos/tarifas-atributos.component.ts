import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// Services
import {
  TarifasAtributosService,
  RequestAuth,
  ToastService,
  LoadingService,
  TiposTarifasService,
  FormatosService
} from '../../services/services.index';

// Interfaces
import {
  AtributosConfigurar,
  ValoresPosibles,
  TiposTarifas,
  ConfiguracionTarifas,
  Formatos
} from '../../interfaces/interfaces.index';

// Config
import { URL_SERVICIOS } from '../../config/config';

// Messages
import { TarifasAtributosMessages } from '../../messages/messages.index';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

declare var $;

@Component({
  selector: 'app-tarifas-atributos',
  templateUrl: './tarifas-atributos.component.html',
  styles: []
})
export class TarifasAtributosComponent implements OnInit {

  url: string = URL_SERVICIOS;

  // Constantes
  formatoTarifa: number = 0;
  atributoConfigurar: number = 0;
  datosAtributosAdm: any = [];
  btnSave: string;
  chkExcluyente: boolean = false;
  chkAttrValor: boolean = false;
  boolExcluyente: string = "No";

  // Arreglos
  atributosConfigurar: AtributosConfigurar [] = [];
  formatosTarifas: Formatos [] = [];
  tiposTarifas: TiposTarifas [] = [];
  valoresPosibles: ValoresPosibles [] = [];

  // Loading
  searchAtributosConfigurar: string = 'success';
  searchConfiguracionTarifas: string = 'success';
  searchFomatosTarifas: string = 'success';
  searchTiposTarifas: string = 'success';
  searchValoresPosibles: string = 'success';


  // Formularios
  formulario: FormGroup;

  // Disabled
  filedsDisabled: boolean = true;
  excluyenteDisabled: boolean = true;
  adicionalDisabled: boolean = true;
  saveDisabled: boolean = true;
  expresionDisabled: boolean = false;


  // Variables Seteadas
  configuracionTarifa: ConfiguracionTarifas = {
    adicional: false,
    excluyente: false,
    expmaxima: '',
    expminima: '',
    tarifas: [],
    tipo_tarifa: '',
    valmaximo: '',
    valminimo: '',
    valores: [],
  };

  // messages
  messages: any;

  constructor(
    private _tarifasAtributosServices: TarifasAtributosService,
    private _requestAuth: RequestAuth,
    private _toastService: ToastService,
    private _loadingService: LoadingService,
    private _tiposTarifasService: TiposTarifasService,
    private _formatosService: FormatosService
  ) {

    this.getFormatosTarifas();

    this.getTiposTarifas();

    this.validaciones();

  }


  ngOnInit() {
    $( "#valoresPosibles tbody" ).sortable({
      update: this.reordenar
    });
    $( "#valoresPosibles tbody" ).disableSelection();
  }

  reordenar() {
    $("#tbodyValoresPosibles tr").each( function(index) {
      index++;
      let inputOrd: string = "#ord_" + this.id.substring(2, this.id.length);
      $(inputOrd).html('');
      $(inputOrd).html(index);
    });
  }

  getFormatosTarifas() {

    this.searchFomatosTarifas = 'loading';

    this._formatosService.getFormatos( true ).subscribe( (resp: any) => {

      this.searchFomatosTarifas = 'success';

      this.formatosTarifas = resp.data;

    }, () => {

      this.searchFomatosTarifas = 'error';

    });

  }

  getAtributosConfigurar( formatoTarifa: number) {

    this.filedsDisabled = true;

    this.validaciones();

    this.formulario.controls['codigo_formato'].setValue( formatoTarifa );

    this.atributoConfigurar = 0;

    this.atributosConfigurar = [];

    this.valoresPosibles = [];

    this.formatoTarifa = formatoTarifa;

    if ( this.formatoTarifa != 0 ) {

      this.searchAtributosConfigurar = 'loading';

      this._tarifasAtributosServices.getAtributosConfigurar( formatoTarifa ).subscribe( (resp: any) => {

        this.searchAtributosConfigurar = 'success';

        this.atributosConfigurar = resp.data;

      }, () => {

        this.searchAtributosConfigurar = 'error';

      });

    }

  }

  resetExpresiones() {
    this.formulario.controls['expresion_min'].reset();
    this.formulario.controls['expresion_max'].reset();
    this.formulario.controls['valor_min'].reset();
    this.formulario.controls['valor_max'].reset();
  }

  getValoresPosibles( atributoConfigurar: number ) {

    this.resetExpresiones();

    this.filedsDisabled = true;

    this.formulario.controls[ 'atributo_id' ].setValue( atributoConfigurar );

    this.valoresPosibles = [];

    this.atributoConfigurar = atributoConfigurar;

    if ( this.formatoTarifa != 0 && this.atributoConfigurar != 0 ) {

      this.searchValoresPosibles = 'loading';

      this._tarifasAtributosServices.getValoresPosibles( this.formatoTarifa, this.atributoConfigurar ).subscribe( (resp: any) => {

        this.searchValoresPosibles = 'success';

        this.prepararDatos(resp.data);

        this.valoresPosibles = this.datosAtributosAdm;

        this.buscarConfiguracion();

      }, () => {

        this.searchValoresPosibles = 'error';

      });

    }

  }

  activarFormulario( unValor?: boolean ) {

    if ( unValor && this.valoresPosibles.length == 1) {
      $("#check_" + this.valoresPosibles[0].id ).prop('checked', true);
      $("#check_" + this.valoresPosibles[0].id ).prop('disabled', true);
    }

    let attrValorChk = $('.chkAttrValor:checked').length;
    this.filedsDisabled = true;
    this.excluyenteDisabled = true;
    this.adicionalDisabled = false;

    if ( attrValorChk > 0 ) {

      this.filedsDisabled = false;

    }

    let existe = this.tiposTarifas.find( x => x.rango === true);
    let indexTipoT = this.tiposTarifas.map(function(e) { return e.codigo; }).indexOf(existe.codigo);
    this.tiposTarifas[indexTipoT].hidden = false;

    if ( this.valoresPosibles.length > 1 ) {

      this.formulario.controls['excluyente'].setValue(true);
      this.chkExcluyente = true;
      this.tiposTarifas[indexTipoT].hidden = true;

    } else {

      this.chkExcluyente = false;

    }
    this.adicionalDisabled = ( this.chkExcluyente ) ? false : true;
    this.expresionDisabled = ( this.chkExcluyente ) ? true : false;

    this.validarForm();
  }

  maxValue( event ) {

    let valor: string = ( event.checked ) ? 'Max Value' : '';

    this.formulario.controls[ event.name ].setValue( valor );
  }

  validarForm( aut: number = 0) {

    this.saveDisabled = true;

    if ( aut == 1 ) {
      if (this.configuracionTarifa.valores.length > 0) {
          this.saveDisabled = false;
        }
    } else if ( aut == 2 ) {
      this.saveDisabled = true;
    } else {
      let tipoTarChk = $('.chkTipoTarifa:checked').length;
      if (tipoTarChk > 0) {
          this.saveDisabled = false;
        }
    }
  }

  toggleInfAd( event ) {

    let checked: boolean =  ( event.checked ) ? true : false;

    this.adicionalDisabled = checked;
    this.expresionDisabled = checked;

    this.resetExpresiones();

  }

  prepararDatos( datos: any ) {

    if ( datos.length == 1) {
      if (datos[0].vl_atributo_valor == "Null") {
        datos[0].vl_atributo_valor = $("#atributosConfigurar option:selected").text();
      }
    }

    this.datosAtributosAdm = datos;

  }

  getTiposTarifas() {

    this.searchTiposTarifas = 'loading';

    this._tiposTarifasService.getTiposTarifas().subscribe( (resp: any) => {

      this.tiposTarifas = resp.data;

      // this.addHiddenField( resp.data );

      this.searchTiposTarifas = 'success';

    }, () => {

      this.searchTiposTarifas = 'error';

    });

  }

  addHiddenField( data ) {

    data.forEach((elemento, key) => {
      data[key].hidden = false;
    });

    this.tiposTarifas = data;

  }

  validaciones() {

    this.formulario = new FormGroup({
      'expresion_min': new FormControl(false),
      'expresion_max': new FormControl(false),
      'valor_min': new FormControl(false),
      'valor_max': new FormControl(false),
      'excluyente': new FormControl(false),
      'info_adicional': new FormControl(false),
      'codigo_formato': new FormControl('', [Validators.required]),
      'atributo_id': new FormControl('', [Validators.required]),
    });

    let messages = new TarifasAtributosMessages();
    this.messages = messages.getMessages();

  }

  save() {

    if ( this.arrayTiposTarifas() == true || this.arrayValores() == true ) {
      return;
    }

    this._loadingService.loadingIn();

    let data: any = {
      datos: [
        {
          formato_id: this.formulario.value.codigo_formato,
          atributo_id: this.formulario.value.atributo_id,
          excluyente: (this.formulario.value.excluyente) ? true : false,
          inf_adicional: (this.formulario.value.info_adicional) ? true : false,
          expminima: this.formulario.value.expresion_min,
          expmaxima: this.formulario.value.expresion_max,
          valminimo: this.formulario.value.valor_min,
          valmaximo: this.formulario.value.valor_max,
          tarifas: this.arrayTiposTarifas(),
          valores: this.arrayValores(),
        },
      ]
    };

    this._requestAuth.post( this.url + 'configuraciontarifaadm', data ).subscribe( (resp: any) => {

      this._loadingService.laodingOut();

      this._toastService.showSuccess('¡Éxito!', 'Registro guardado exitosamente.');

    }, (error) => {

      this._loadingService.laodingOut();

      this._toastService.errores( error );

    });

  }

  buscarConfiguracion() {

    this.searchConfiguracionTarifas = 'loading';
    this.chkExcluyente = false;

    this.setConfiguracionTarifa();

    this._tarifasAtributosServices.getConfiguracionTarifa( this.formatoTarifa, this.atributoConfigurar ).subscribe( (resp: any) => {

      this.searchConfiguracionTarifas = 'success';

      if ( resp.data != 'Null' ) {

        this.filedsDisabled = false;

        this.configuracionTarifa = resp[0];

        this.formulario.controls['expresion_min'].setValue(this.configuracionTarifa.expminima);
        this.formulario.controls['expresion_max'].setValue(this.configuracionTarifa.expmaxima);
        this.formulario.controls['valor_min'].setValue(this.configuracionTarifa.valminimo);
        this.formulario.controls['valor_max'].setValue(this.configuracionTarifa.valmaximo);
        this.formulario.controls['excluyente'].setValue(this.configuracionTarifa.excluyente);
        this.formulario.controls['info_adicional'].setValue(this.configuracionTarifa.adicional);
        this.chkExcluyente = this.configuracionTarifa.excluyente;

        this.setValores( this.configuracionTarifa.valores );

        this.activarFormulario();

        this.validarForm(2);

        setTimeout( () => {
          this.setTipoTarifa( this.configuracionTarifa.tarifas );
        }, 10);

      } else {
        this.activarFormulario( true );
      }

    }, () => {

      this.searchConfiguracionTarifas = 'error';

    });

  }

  setConfiguracionTarifa() {

    this.formulario.controls['expresion_min'].setValue('');
    this.formulario.controls['expresion_max'].setValue('');
    this.formulario.controls['valor_min'].setValue('');
    this.formulario.controls['valor_max'].setValue('');
    this.formulario.controls['excluyente'].setValue('');
    this.formulario.controls['info_adicional'].setValue('');

    this.configuracionTarifa = {
      adicional: false,
      excluyente: false,
      expmaxima: '',
      expminima: '',
      tarifas: [],
      tipo_tarifa: '',
      valmaximo: '',
      valminimo: '',
      valores: [],
    };

  }

  arrayTiposTarifas() {

    let array: any = [];

    let validation: boolean = true;

    for (let i = 0; i < this.tiposTarifas.length; i++ ) {

      if ( $('#tipoTarifa_' + this.tiposTarifas[i].codigo).is(':checked') ) {

        let tarifa: any = {
          tipo_tarifa_id: this.tiposTarifas[i].codigo
        };

        array.push(tarifa);

        validation = false;

      }

    }

    if ( validation ) {

      this._toastService.showWarning('¡Aviso!', 'Debe seleccionar al menos un tipo de tarifa.');
      return true;

    }

    return array;

  }

  arrayValores() {

    let array: any = [];

    let validation: boolean = true;

    for (let i = 0; i < this.valoresPosibles.length; i++ ) {

      if ( $('#check_' + this.valoresPosibles[i].id).is(':checked') ) {

        let valor: any = {
          codigo_atributo_valor: this.valoresPosibles[i].id,
          orden: $('#ord_' + this.valoresPosibles[i].id).html()
        };

        array.push(valor);

        validation = false;

      }

    }

    if ( validation ) {

      this._toastService.showWarning('¡Aviso!', 'Debe seleccionar al menos un valor.');
      return true;

    }

    return array;

  }

  setValores( valores: any ) {

    let arrReal: any = [];
    let valoresAttr: any = this.datosAtributosAdm;
    let valorExiste: any = [];
    let arrTemp: any = [];

    for (let i = 0; i <  valoresAttr.length; i++) {

      let existe = valores.find( x => x.codigo_atributo_valor === valoresAttr[i].id);

      if (existe) {

        let indexAut: number = existe.orden;
        indexAut--;

        if (valorExiste[indexAut]) {
          arrTemp = [];
          arrTemp.push(valorExiste[indexAut]);
          valorExiste[indexAut] = valoresAttr[i];
          valorExiste.push(arrTemp[0]);

        } else {
          valorExiste[indexAut] = valoresAttr[i];
        }

      } else {

        for ( let k = 0, j = valoresAttr.length ; k < j; k++ ) {
          if (!valorExiste[k]) {
            valorExiste[k] = valoresAttr[i];
            break;
          }
        }
      }
    }

    valorExiste.forEach(function (value) {
      arrReal.push(value);
    });

    this.valoresPosibles = arrReal;

    for ( let i = 0; i < valores.length; i++ ) {

      $("#check_" + valores[i].codigo_atributo_valor).prop('checked', true);

    }

  }

  setTipoTarifa( tarifas: any ) {

    for ( let i = 0; i < tarifas.length; i++ ) {

      $("#tipoTarifa_" + tarifas[i].tipo_tarifa_id).prop('checked', true);

    }

  }

}
