import { RouterModule, Routes } from '@angular/router';

// Components
import { DashboardComponent } from './dashboard.component';

// Guards
import {
    VerificaTokenGuard
} from '../../guards/guards.index';

const dashboardRoutes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        canActivate: [
            // VerificaTokenGuard
        ]
    }
];

export const DASHBOARD_ROUTES = RouterModule.forChild( dashboardRoutes );
