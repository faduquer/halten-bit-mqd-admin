import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DASHBOARD_ROUTES } from './dashboard.routes';
import { DashboardComponent } from './dashboard.component';

@NgModule({
    declarations: [
        DashboardComponent
    ],
    exports: [
        DashboardComponent
    ],
    imports: [
        DASHBOARD_ROUTES,
        CommonModule
    ]
})

export class DashboardModule { }
