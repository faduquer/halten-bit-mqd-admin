import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Rutas
import { TARIFAS_FORMATOS_ROUTES } from './tarifas-formatos.routes';

// Components
import { TarifasFormatosComponent } from './tarifas-formatos.component';

@NgModule({
    declarations: [
        TarifasFormatosComponent
    ],
    exports: [
        TarifasFormatosComponent
    ],
    imports: [
        TARIFAS_FORMATOS_ROUTES,
        CommonModule,
        FormsModule,
        ReactiveFormsModule

    ]
})

export class TarifasFormatosModule { }
