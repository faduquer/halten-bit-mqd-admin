import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';

// Config
import { URL_SERVICIOS } from '../../config/config';

// Interfaces
import {
  Formatos,
  TarifasConfiguradas,
  ConfiguracionTarifa
} from '../../interfaces/interfaces.index';

// Services
import {
  FormatosService,
  TarifasFormatosService,
  ConfiguracionTarifaService,
  ToastService,
  RequestAuth,
  LoadingService
} from '../../services/services.index';

// Messages
import { TarifasFormatosMessages } from '../../messages/messages.index';

declare var $;

@Component({
  selector: 'app-tarifas-formatos',
  templateUrl: './tarifas-formatos.component.html',
  styles: [],
  providers: [DatePipe]
})
export class TarifasFormatosComponent implements OnInit {

  addConfiguraciones: ConfiguracionTarifa [] = [];

  configuraciones: ConfiguracionTarifa [] = [];

  formatos: Formatos [] = [];

  tarifasTipos_: any;

  tarifasTipos: any;

  validacionTipo: any;

  validacionTipoTarifa: any;

  valTipo: any;

  formulario: FormGroup;

  hoy = this._datePipe.transform( new Date, 'dd-MM-yyyy' );

  disRadio: boolean = true;

  messages: any;

  disabledSave = false;

  tarifasConfiguradas: TarifasConfiguradas [] = [];

  url: string = URL_SERVICIOS;

  // loading
  searchAddConfiguracion: string = 'success';
  searchConfiguracion: string = 'success';
  searchFomatos: string = 'success';
  searchTarifasConfiguradas: string = 'success';

  constructor(
    private _formatosService: FormatosService,
    private _tarifasFormatosServices: TarifasFormatosService,
    private _configuracionTarifaService: ConfiguracionTarifaService,
    private _toastService: ToastService,
    private _requestAuth: RequestAuth,
    private _loadingService: LoadingService,
    private _datePipe: DatePipe,
  ) {

    this.getFormatos();

    this.getTarifasConfiguradas();

    this.validaciones();

  }

  ngOnInit() {}

  getFormatos() {

    this.searchFomatos = 'loading';

    this._formatosService.getFormatos().subscribe( (resp: any) => {

      this.searchFomatos = 'success';

      this.formatos = resp.data;

    }, () => {

      this.searchFomatos = 'error';

    });

  }

  getTarifasConfiguradas() {

    this.searchTarifasConfiguradas = 'loading';

    this._tarifasFormatosServices.getTarifasConfiguradas().subscribe( (resp: any) => {

      this.searchTarifasConfiguradas = 'success';

      this.tarifasConfiguradas = resp.data;

      this.validaciones();

    }, () => {

      this.searchTarifasConfiguradas = 'error';

    });

  }

  getConfiguracion( id: number ) {

    this.searchAddConfiguracion = 'loading';

    if ( id > 0 ) {

      this._configuracionTarifaService.getConfiguraciones( id ).subscribe( (resp: any) => {

        this.searchAddConfiguracion = 'success';

        this.addConfiguraciones = resp.data;

        this.getRowsConfiguracionesSelect ( resp.data );

        for ( let i = 0; i < this.configuraciones.length; i++ ) {
          this.tarifasTipos_ = this.configuraciones[i].tipo_tarifa_plataforma_id;
          this.tarifasTipos = this.configuraciones[i].tarifa_plataforma_id;
        }

      }, () => {

        this.searchAddConfiguracion = 'error';

      });


    } else {

      this.searchAddConfiguracion = 'success';

      this.addConfiguraciones = [];

    }

    /*setTimeout(() => {
      console.log("#tipoTarifa" + this.tarifasTipos_);
      $("#tipoTarifa" + this.tarifasTipos_).prop('checked', true);
    }, 500);*/

  }

  addConfiguracionTarifas() {

    if ( this.configuraciones.length > 0 ) {

      let existe: any = this.configuraciones.filter( x => x.tarifa_plataforma_id == this.addConfiguraciones[0].tarifa_plataforma_id );

      if ( existe.length > 0 ) {

        this.valTipo = this.configuraciones.find( x => x.tarifa_plataforma_id == this.addConfiguraciones[0].tarifa_plataforma_id );

        if (this.valTipo.tipo_tarifa_plataforma.rango) {

          let tarifaId: number = this.valTipo.tarifa_plataforma_id;
          let tipoT: any = this.configuraciones.filter(function(e) {
            return e.tarifa_plataforma_id == tarifaId;
          });

          tipoT.forEach(elemento => {
            let indexTipoT = this.configuraciones.map(function(e) { return e.id; }).indexOf(elemento.id);
            this.configuraciones.splice(indexTipoT, 1);
          });

        } else {

          let indexTipoT = this.configuraciones.map(function(x) { return x.tarifa_plataforma_id; }).indexOf(this.valTipo.tarifa_plataforma_id);
          this.configuraciones.splice(indexTipoT, 1);

        }


      }

    }


    $('#modalNuevaTarifa').modal('hide');

    $('#plataforma').val('0');

    this.searchConfiguracion = 'loading';

    for (let i = 0; i < this.addConfiguraciones.length; i++ ) {

      if ( $('#tipoTarifa' + this.addConfiguraciones[i].tipo_tarifa_plataforma_id).is(':checked') ) {
        this.configuraciones.push( this.addConfiguraciones[i] );
      }

    }

    this.getRowsConfiguraciones ( this.configuraciones );

    this.searchConfiguracion = 'success';

    this.setFormControl();

    this.addConfiguraciones = [];


    setTimeout(() => {
      this.setFecha();
      this.setDataFecha();
    }, 10);

  }

  setFecha() {

    $('.fecha, .fechaDesde').datepicker({
      autoclose: true,
      todayHighlight: true,
      language: 'es',
      clearBtn: true,
      startDate: this._datePipe.transform( new Date, 'dd-MM-yyyy' ),
    });

  }

  save() {

    let formato: number = $('#formato').val();

    if ( formato == 0 ) {

      this._toastService.showWarning('¡Aviso!', 'Debe seleccionar un formato, por favor');
      return;

    }

    let data: any = {
      datos: this.arrayValoresConfiguracion()
    };

    this._loadingService.loadingIn();

    this._requestAuth.post( this.url + 'formato/' + formato + '/configuraciontarifaformato', data ).subscribe( (resp: any) => {

      this._loadingService.laodingOut();

      this._toastService.showSuccess('¡Éxito!', 'Datos guardados con éxito!');

      this.getFormatoConfiguracion( formato );

      this.disRadio = false;

    }, (error) => {

      this._loadingService.laodingOut();

      this._toastService.errores( error );

    });

  }

  arrayValoresConfiguracion() {

    let array: any = [];
    let fechaHasta: string;
    let fechaDesde: string;

    this.configuraciones.forEach((elemento, key) => {

      if (elemento.show_titulo) {

        fechaHasta = $('#hasta' + key).val();
        fechaDesde = $('#desde' + key).val();

      }

      let data: any = {
        codigo_tarifa: elemento.id,
        tarifa: $('#tarifa' + key).val(),
        desde: this.cambiarFormatoFecha(fechaDesde),
        hasta: this.cambiarFormatoFecha(fechaHasta)
      };

      array.push( data );

    });

    return array;

  }

  cambiarFormatoFecha( date: string ) {

    let fecha = '';

    if ( date ) {

      let info = date.split('-');
      fecha = info[2] + '-' + info[1] + '-' + info[0];

    }

    return fecha;

  }

  validaciones() {

    let formato: any = '';

    if ( $('#formato').val() != '' && $('#formato').val() != undefined ) {

      formato = $('#formato').val();

    }

    this.formulario = new FormGroup({
      'formato': new FormControl(formato, [Validators.required])
    });

    let messages = new TarifasFormatosMessages();
    this.messages = messages.getMessages();

  }

  getToday() {
    let today = new Date();
    let getFecha: any = today.getDate() + '-' + ( today.getMonth() + 1 ) + '-' + today.getFullYear();
    return getFecha;
  }

  setFormControl() {

    this.disabledSave = false;
    this.validaciones();

    let messages = new TarifasFormatosMessages();
    this.messages = messages.setValidators( this.configuraciones );

    this.configuraciones.forEach((elemento, key) => {

      this.disabledSave = true;

      let tarifa: FormControl = new FormControl(elemento.tarifa);
      this.formulario.addControl( "tarifa" + key, tarifa );
      this.formulario.controls[ "tarifa" + key ].setValidators([Validators.required]);

      if (elemento.show_titulo) {

        let desde: FormControl = new FormControl(this.cambiarFormatoFecha(elemento.aplica_desde));
        this.formulario.addControl( "desde" + key, desde );
        this.formulario.controls[ "desde" + key ].setValidators([Validators.required, Validators.pattern('(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}')]);

        if (!this.cambiarFormatoFecha(elemento.aplica_desde)) {
          this.formulario.controls[ "desde" + key ].setValue(this.getToday());
        } else {
          this.formulario.controls[ "desde" + key ].setValue(this.cambiarFormatoFecha(elemento.aplica_desde));
        }

        let hasta: FormControl = new FormControl(this.cambiarFormatoFecha(elemento.aplica_hasta));
        this.formulario.addControl( "hasta" + key, hasta );
        this.formulario.controls[ "hasta" + key ].setValidators([Validators.pattern('(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}')]);

      }

    });

  }

  deleteConfiguracion( tipoTarifaId: number, rango: any ) {

    if ( confirm("Seguro que desea eliminar?") ) {

      if ( !rango ) {
        let indexTipoT = this.configuraciones.map(function(e) { return e.tipo_tarifa_plataforma_id; }).indexOf(tipoTarifaId);
        this.configuraciones.splice(indexTipoT, 1);

      } else {

        let tipoT: any = this.configuraciones.filter(function(e) {
          return e.tipo_tarifa_plataforma_id == tipoTarifaId;
        });

        tipoT.forEach(elemento => {
          let indexTipoT = this.configuraciones.map(function(e) { return e.id; }).indexOf(elemento.id);
          this.configuraciones.splice(indexTipoT, 1);
        });

      }

      this.setFormControl();
    }

  }

  getFormatoConfiguracion( id: any ) {

    this.configuraciones = [];
    this.disRadio = true;

    if ( id != '' ) {

      this.searchConfiguracion = 'loading';

      this._tarifasFormatosServices.getFormatoConfiguraciones( id ).subscribe( (resp: any) => {

        this.searchConfiguracion = 'success';

        this.getRowsConfiguraciones(resp);

        if (this.configuraciones.length > 0) {
          this.disRadio = false;
        }

        this.setFormControl();

        setTimeout(() => {
          this.setFecha();
        }, 10);

      }, () => {

        this.searchConfiguracion = 'error';

      });

    } else {

      this.validaciones();

    }

  }

  getRowsConfiguraciones ( data ) {

    let rowspan: number;

    data.forEach((elemento, key) => {

      rowspan = 0;

      if (elemento.show_titulo) {

        let rows: any = data.filter(function(e) {
          return e.tarifa_plataforma_id == elemento.tarifa_plataforma.id;
        });

        rowspan = rows.length;
      }

      data[key].rowspan = rowspan;
    });

    this.configuraciones = data;

  }

  getRowsConfiguracionesSelect ( data ) {

    let rowspan: number;

    data.forEach((elemento, key) => {

      rowspan = 0;

      if (elemento.show_titulo) {
        let rows: any = data.filter(function(e) {
          return e.tipo_tarifa_plataforma_id == elemento.tipo_tarifa_plataforma_id;
        });

        rowspan = rows.length;
      }

      data[key].rowspan = rowspan;
    });

    this.addConfiguraciones = data;

  }

  setDataFecha() {

    let form = this.formulario;

    $('.fecha, .fechaDesde').change(function() {

      let value = $(this).val();

      let id = $(this).attr("id");

      form.controls[id].setValue(value);

    });

  }

  activarTarifa( idConfTarifa ) {

    let formato: number = $('#formato').val();

    if ( formato == 0 ) {

      this._toastService.showWarning('¡Aviso!', 'Debe seleccionar un formato, por favor');
      return;

    }

    let data: any = {
      activo: true,
    };

    // this._loadingService.loadingIn();

    this._requestAuth.put( this.url + 'formato/' + formato + '/configuraciontarifaformato/' + idConfTarifa, data ).subscribe( (resp: any) => {

      // this._loadingService.laodingOut();

      this._toastService.showSuccess('¡Éxito!', 'Tarifa activada!');

    }, (error) => {

      // this._loadingService.laodingOut();

      this._toastService.errores( error );

    });
  }
}
