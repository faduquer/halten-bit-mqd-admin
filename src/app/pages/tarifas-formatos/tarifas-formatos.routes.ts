import { RouterModule, Routes } from '@angular/router';

// Components
import { TarifasFormatosComponent } from './tarifas-formatos.component';

// Guards
import {
    VerificaTokenGuard
} from '../../guards/guards.index';

const tarifasFormatosRoutes: Routes = [
    {
        path: '',
        component: TarifasFormatosComponent,
        canActivate: [
            VerificaTokenGuard
        ]
    }
];

export const TARIFAS_FORMATOS_ROUTES = RouterModule.forChild( tarifasFormatosRoutes );
