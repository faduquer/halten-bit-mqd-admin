import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routes
import { PAGES_ROUTES } from './pages.routes';

@NgModule({
    declarations: [
    ],
    exports: [
    ],
    imports: [
        PAGES_ROUTES,
        CommonModule
    ]
})

export class PagesModule { }
