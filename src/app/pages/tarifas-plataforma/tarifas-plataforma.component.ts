import { Component, OnInit } from '@angular/core';

// Config
import {
  URL_SERVICIOS,
  DataTableEspañol
} from '../../config/config';

// Services
import {
  RequestAuth,
  RedirectService
} from '../../services/services.index';

declare var $;

@Component({
  selector: 'app-tarifas-plataforma',
  templateUrl: './tarifas-plataforma.component.html',
  styles: []
})
export class TarifasPlataformaComponent implements OnInit {

  url: string = URL_SERVICIOS;

  // Datatable
  urlDataTable = this.url + 'tarifaplataforma';
  storage: any = [];
  table: any;

  constructor(
    private _requestAuth: RequestAuth,
    private _redirectService: RedirectService
  ) { }

  ngOnInit() {

    this.cargar();

  }

  cargar( url: string = this.urlDataTable ) {

    let tok = "Bearer " + this._requestAuth.obtenerTokenLocalStorage();

    this.table = $("#tablaTarifasPlataforma").DataTable({
      "destroy": true,
      "serverSide": true,
      "searching": false,
      "processing": true,
      "stateSave": true,
      "language": DataTableEspañol,
      "ajax": {
        "method": "GET",
        "url": url,
        "headers": {
          'Content-Type': 'application/json',
          'Authorization': tok
        },
        "error": (xhr, error, thrown) => {

          let html = '<div class="container" style="padding: 10px;">';
          html += '<span class="text-danger">Ha ocurrido un errror al procesar la informacion.</span> ';
          html += '<button type="button" class="btn btn-outline-danger" id="refrescar"><i class="fa fa-refresh fa-spin"></i> Refrescar</button>';
          html += '</div>';

          $("#tablaTarifasPlataforma_processing").html(html);

          $("#refrescar").click( () => {
            this.cargar();
          });

        }
      },
      "columns": [
        { "data": "nombre", "name": "nombre" },
        { "data": "descripcion", "name": "descripcion" },
        { "data": "vigencia", "name": "vigencia",
          render: (data, type, row) => {

            if ( data ) {

              return '<i class="fa fa-check-circle-o text-success" aria-hidden="true"></i>';

            } else if ( !data ) {

              return '<i class="fa fa-times-circle-o text-danger" aria-hidden="true"></i>';

            }

          }
        },
        { "data": null, 'sortable': false,
          render: (data, type, row) => {

            return '<button class="btn btn-primary editar">Editar</button>';

          }
        },
      ]
    });

    this.editar('#tablaTarifasPlataforma tbody', this.table);

  }

  editar(tbody: any, table: any, that = this) {

    $(tbody).on('click', 'button.editar', function () {
      let data = table.row( $(this).parents("tr") ).data();
      that._redirectService.editarTarifaPlataforma( data.id );
    });

  }

}
