import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Rutas
import { TARIFAS_PLATAFORMA_ROUTES } from './tarifas-plataforma.routes';

// Components
import { TarifasPlataformaComponent } from './tarifas-plataforma.component';

@NgModule({
    declarations: [
        TarifasPlataformaComponent
    ],
    exports: [
        TarifasPlataformaComponent
    ],
    imports: [
        TARIFAS_PLATAFORMA_ROUTES,
        CommonModule,
        FormsModule,
        ReactiveFormsModule

    ]
})

export class TarifasPlataformaModule { }
