import { RouterModule, Routes } from '@angular/router';

// Components
import { TarifaPlataformaComponent } from './tarifa-plataforma.component';

// Guards
import {
    VerificaTokenGuard
} from '../../../guards/guards.index';

const tarifaPlataformaRoutes: Routes = [
    {
        path: '',
        component: TarifaPlataformaComponent,
        canActivate: [
            VerificaTokenGuard
        ]
    }
];

export const TARIFA_PLATAFORMA_ROUTES = RouterModule.forChild( tarifaPlataformaRoutes );
