import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Rutas
import { TARIFA_PLATAFORMA_ROUTES } from './tarifa-plataforma.routes';

// Components
import { TarifaPlataformaComponent } from './tarifa-plataforma.component';

@NgModule({
    declarations: [
        TarifaPlataformaComponent
    ],
    exports: [
        TarifaPlataformaComponent
    ],
    imports: [
        TARIFA_PLATAFORMA_ROUTES,
        CommonModule,
        FormsModule,
        ReactiveFormsModule

    ]
})

export class TarifaPlataformaModule { }
