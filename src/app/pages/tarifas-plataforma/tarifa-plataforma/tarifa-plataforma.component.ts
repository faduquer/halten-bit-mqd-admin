import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

// Services
import {
  RedirectService,
  TarifasPlataformaService,
  ToastService,
  LoadingService,
  TiposTarifasService,
  RequestAuth,
  ConfiguracionTarifaService,
} from '../../../services/services.index';

// Interfaces
import {
  TarifasPlataforma,
  TiposTarifas,
  ConfiguracionTarifa
} from '../../../interfaces/interfaces.index';

// Config
import { URL_SERVICIOS } from '../../../config/config';

// Messages
import { TarifasPlataformaMessages } from '../../../messages/messages.index';
import { forEach } from '@angular/router/src/utils/collection';
import { element } from 'protractor';

declare var $;

@Component({
  selector: 'app-tarifa-plataforma',
  templateUrl: './tarifa-plataforma.component.html',
  styles: []
})
export class TarifaPlataformaComponent implements OnInit {

  configuraciones: ConfiguracionTarifa [] = [];

  formulario = new FormGroup({
    nombre: new FormControl(),
    descripcion: new FormControl(),
    vigencia: new FormControl(),
    countRango: new FormControl(),
    aplica: new FormControl(),
  });

  formularioTipoTarifa = new FormGroup({
    tipoTarifa: new FormControl(),
    desde: new FormControl(),
    hasta: new FormControl()
  });

  data = {
    entities: [
      { id: 1,  description: 'Cliente', checked: '' },
      { id: 2,  description: 'Generador', checked: '' },
    ]
  };

 desdeValue: any = "";
 hastaValue: any = "";
 disabledDesde: any = false;
 porcentaje: any = "false";
 modalAddDisabled: any = true;
 modalSaveDisabled: any = true;
 idRangoTarifa: number;
 countRango = false;
 selected: number = 0;
 disaAdd: any = true;
 disaSave: any = true;
 cardConfiguraciones: any = [];
 configuracionesRango: any [] = [];
  messages: any;
  param: string;

  tarifaPlataforma: TarifasPlataforma = {
    codigo: 0,
    descripcion: '',
    fecha_actualizacion: '',
    fecha_creacion: '',
    fecha_eliminacion: '',
    nombre: '',
    vigencia: false,
    aplica: ''
  };

  tiposTarifas: TiposTarifas [] = [];
  tiposTarifasTabla: TiposTarifas [] = [];
  mostrarTarifa: number = 0;
  valTarifa: any;
  idTipoTarifa: number;
  modalTarget: string = "";
  url: string = URL_SERVICIOS;

  // Loading
  searchConfiguracionTarifa: string = 'success';
  searchTiposTarifas: string = 'success';

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _redirectService: RedirectService,
    private _tarifasPlataformaService: TarifasPlataformaService,
    private _toastService: ToastService,
    private _loadingService: LoadingService,
    private _tiposTarifasService: TiposTarifasService,
    private _requestAuth: RequestAuth,
    private _configuracionTarifaService: ConfiguracionTarifaService,
  ) {

    this._activatedRoute.params.subscribe( params => {

      this.param = params['id'];

      if ( this.param != 'new' ) {

        if ( this.param === '' ) {
          this._redirectService.noPageFound();
          return;
        }

        this.getTiposTarifas();

        this._loadingService.loadingIn();

        this._tarifasPlataformaService.getTarifaPlataforma( params['id'] ).subscribe( (resp: any) => {

          this._loadingService.laodingOut();

          this.tarifaPlataforma = resp.data;

          this.validaciones();

          this.getConfiguraciones();

        }, (error) => {

          this._loadingService.laodingOut();

          this._toastService.errores( error );

        });

      }

    });

  }

  ngOnInit() {

    this.validaciones();

  }


  getAplica(event) {

    let data = (  this.tarifaPlataforma.aplica.length > 0 ) ? this.tarifaPlataforma.aplica.split(';') : [];
    let existe = data.indexOf(event.value);

    if ( event.checked) {
      if ( existe < 0 ){
        data.push(event.value);
      }
    } else {
      if ( existe >= 0 ) {
        data.splice(existe, 1);
      }
    }
    this.tarifaPlataforma.aplica = data.join(';');

    this.formulario.controls['aplica'].setValue(this.tarifaPlataforma.aplica);
  }

  closeModal() {
    $("#tipoTarifa option[value=0]").prop("selected", true);
  }

  showModalRango( tipoTarifa ) {
    $('#modalRangos').modal('show');
    this.configuracionesRango = [];
    this.addRango( tipoTarifa );
  }

  getConfiguraciones() {

    this.searchConfiguracionTarifa = 'loading';

    this._configuracionTarifaService.getConfiguraciones( this.tarifaPlataforma.codigo ).subscribe( (resp: any) => {

      this.searchConfiguracionTarifa = 'success';

      this.configuraciones = resp.data;

      this.getCardsConfiguraciones( resp.data );

      this.elimTipoTarifaCombo();

    }, () => {

      this.searchConfiguracionTarifa = 'error';

    });

  }

  getRowsConfiguraciones ( data ) {

    data.forEach((elemento, key) => {

      let dat_tarifa = this.tiposTarifas.find( x => x.codigo == elemento.tipo_tarifa_plataforma_id );

      data[key].rango = dat_tarifa.rango;

    });

    this.configuraciones = data;

  }

  getCardsConfiguraciones ( data ) {

    let card: number;

    this.cardConfiguraciones = [];

    data.forEach((elemento: any, key: any) => {

      if (elemento.tipo_tarifa_plataforma.id != card) {

        card = elemento.tipo_tarifa_plataforma.id;
        let rango: any = [];

        if (elemento.tipo_tarifa_plataforma.rango) {
          let rangos = data.filter( x => x.tipo_tarifa_plataforma.rango === true );

          rangos.forEach(dataRango => {
            rango.push({
              desde: dataRango.desde,
              hasta: dataRango.hasta
            });
          });
        }

        let newDataTipoTarifa: any = {
          id: this.cardConfiguraciones.length,
          tipo_tarifa_plataforma_id: elemento.tipo_tarifa_plataforma.id,
          nombre: elemento.tipo_tarifa_plataforma.nombre,
          descripcion: elemento.tipo_tarifa_plataforma.descripcion,
          valor: elemento.desde,
          esRango: elemento.tipo_tarifa_plataforma.rango,
          rango: rango
        };

        this.cardConfiguraciones.push( newDataTipoTarifa );

      }

    });

  }

  getTiposTarifas() {

    this.searchTiposTarifas = 'loading';

    this._tiposTarifasService.getTiposTarifas().subscribe( (resp: any) => {

      this.tiposTarifas = resp.data;

      this.searchTiposTarifas = 'success';

    }, () => {

      this.searchTiposTarifas = 'error';

    });

  }

  maxValue( event ) {

    let index = event.id.substr(4, ( event.id.length - 4 ));
    let maxChecked = event.checked;
    let inputHasta = "#hastaRango_" + index;

    $(inputHasta).val('');

    if ( maxChecked ) {
      $(inputHasta).val('MaxValue');
      $(inputHasta).prop('disabled', true);
      this.modalAddDisabled = true;
      this.modalSaveDisabled = false;

      $("#btnAddRango").addClass( "bttDisabledPrimary" );
      $("#btnAddRango").removeClass( "btn-primary" );

      $("#btnsaveRango").removeClass( "bttDisabled" );
      $("#btnsaveRango").addClass( "btn-success" );

    } else {
      $(inputHasta).prop('disabled', false);
      this.modalSaveDisabled = true;
      $("#btnsaveRango").addClass( "bttDisabled" );
      $("#btnsaveRango").removeClass( "btn-success" );
    }
  }

  disabledInputs( index: number , type: string ) {

    let nombreIconDelete: string = "";
    let nombrechkMax: string = "";
    let cont: number;
    let inputHastaAnt: string = "";
    let inputDesdeAnt: string = "";
    this.modalAddDisabled = true;
    this.modalSaveDisabled = true;

    $("#btnAddRango").addClass( "bttDisabledPrimary" );
    $("#btnAddRango").removeClass( "btn-primary" );

    $("#btnsaveRango").addClass( "bttDisabled" );
    $("#btnsaveRango").removeClass( "btn-success" );

    $(".trashRango, .maxValue").css("display", "none");

    if ( type == 'dis' ) {

      nombreIconDelete = "#del_" + index;
      $(nombreIconDelete).css("display", "block");

    } else {

      cont = index;
      cont--;
      nombreIconDelete = "#del_" + cont;
      $(nombreIconDelete).css("display", "block");

      nombrechkMax = "#groupMax_" + cont;
      $(nombrechkMax).css("display", "block");

      inputHastaAnt = "#hastaRango_" + cont;
      $(inputHastaAnt).prop('disabled', false);

      if (index == 1) {

        inputDesdeAnt = "#desdeRango_" + cont;
        $(inputDesdeAnt).prop('disabled', false);

      }

    }

    this.configuracionesRango.forEach((elemento, key) => {

      if ( type == 'dis' ) {
          if (key < index) {

            inputDesdeAnt = "#desdeRango_" + key;
            inputHastaAnt = "#hastaRango_" + key;

            $(inputDesdeAnt).prop('disabled', true);
            $(inputHastaAnt).prop('disabled', true);

          }
      }

    });

  }

  addRango( codigoTarifa: number ) {

    let indexTipo = this.configuracionesRango.length;
    let desde: number = 1;
    let hasta: number;
    let hastaAnterior: number;
    let validation: boolean = true;
    this.modalSaveDisabled = true;

    if ( indexTipo > 0 ) {

      let nombreInputHastaAnterior = "#hastaRango_" + (this.configuracionesRango.length - 1);
      hastaAnterior = $(nombreInputHastaAnterior).val();

      hasta = hastaAnterior;
      hastaAnterior++;

      desde = hastaAnterior;


      let desdeAnt = $("#desdeRango_" + (this.configuracionesRango.length - 1)).val();
      let hastaAnt = $("#hastaRango_" + (this.configuracionesRango.length - 1)).val();

      this.configuracionesRango[ ( indexTipo - 1 ) ].desde = desdeAnt;
      this.configuracionesRango[ ( indexTipo - 1 ) ].hasta = hastaAnt;

    }

    let newConfig: any = {
      codigo: indexTipo,
      codigo_tipo_tarifa: codigoTarifa,
      desde: desde,
      hasta: ''
    };

    this.configuracionesRango.push( newConfig );

    if ( this.configuracionesRango.length == 1) {
      this.disabledDesde = false;
    }

    if (this.configuracionesRango.length > 1) {

      this.disabledDesde = true;

      this.disabledInputs( indexTipo, 'dis' );

    }

  }

  validaDatos( event ) {

    let index = event.id.substr(11, ( event.id.length - 11 ));
    let inputHastaActual: any = "";
    let inputDesdeActual: any = "";
    let datosHasta: number;
    let datosDesde: number;
    let habInput = true;
    this.modalAddDisabled = true;

    inputHastaActual = "#hastaRango_" + index;
    inputDesdeActual = "#desdeRango_" + index;

    let valHasta = $(inputHastaActual).val();
    let valDesde = $(inputDesdeActual).val();

    datosHasta = parseInt(valHasta, 10);
    datosDesde = parseInt(valDesde, 10);

    if ( !datosDesde ) {
      habInput = false;
    } else if ( !datosHasta ) {
      habInput = false;
    } else if ( datosDesde < 0 ) {
      this._toastService.showError('¡Error!', 'El valor "Desde" debe ser mayor que 0' );
      habInput = false;
    } else if ( datosDesde >= datosHasta ) {
      this._toastService.showError('¡Error!', 'El valor "Hasta" debe ser mayor que ' + datosDesde );
      habInput = false;
    }

    if ( habInput ) {
      this.modalAddDisabled = false;
      $("#btnAddRango").removeClass( "bttDisabledPrimary" );
      $("#btnAddRango").addClass( "btn-primary" );

      this.modalSaveDisabled = false;
      $("#btnsaveRango").removeClass( "bttDisabled" );
      $("#btnsaveRango").addClass( "btn-success" );

    } else {
      $("#btnAddRango").removeClass( "btn-primary" );
      $("#btnAddRango").addClass( "bttDisabledPrimary" );

      this.modalSaveDisabled = true;
      $("#btnsaveRango").removeClass( "btn-success" );
      $("#btnsaveRango").addClass( "bttDisabled" );
    }

  }

  deleteRango( idRango ) {

    let indexAnterior: number;

    this.modalSaveDisabled = true;
    $("#btnsaveRango").addClass( "bttDisabled" );
    $("#btnsaveRango").removeClass( "btn-success" );

    if ( idRango > 0) {
      this.configuracionesRango.splice(idRango, 1);

      this.disabledInputs( idRango, 'ena' );

    }

  }

  saveRango( tipoTarifa ) {

    this.valTarifa = this.tiposTarifas.find( x => x.codigo == tipoTarifa );

    let indexTipoT = this.tiposTarifas.map(function(e) { return e.codigo; }).indexOf( tipoTarifa );

    if (indexTipoT >= 0 ) {

      this.valTarifa = this.tiposTarifas.find( x => x.codigo == tipoTarifa);
      this.tiposTarifasTabla.push( this.valTarifa );

      this.tiposTarifas.splice(indexTipoT, 1);

      let ultimoHasta = $("#hastaRango_" + (this.configuracionesRango.length - 1 )).val();
      this.configuracionesRango[ ( this.configuracionesRango.length - 1 ) ].hasta = ultimoHasta;

      this.configuracionesRango.forEach((elemento, key) => {

        this.addConfiguracion(true, elemento, key);
      });

      this.getCardsConfiguraciones ( this.configuraciones );
      $('#modalRangos').modal('hide');
      this.disaSave = true;
      this.validaciones();

    } else {

      this._toastService.showError('¡Error!', 'Ya ha sido registrado ese tipo de tarifa' );

    }

  }

  addConfiguracion( rango: boolean, data: any = [], key? ) {
    let newConfig: any;
    let idTipoTarPlataforma: number;
    let titulo = true;

    if ( rango ) {
      idTipoTarPlataforma = this.idRangoTarifa;
    } else {
      idTipoTarPlataforma = data.tipoTarifa;
    }

    titulo = (key !== 'undefined' && key > 0) ? false : true;

    newConfig = {
      id: this.configuraciones.length,
      desde: data.desde,
      hasta:  data.hasta,
      show_titulo: titulo,
      show_icon: false,
      tarifa_plataforma: {
        id: this.tarifaPlataforma.codigo,
        nombre: this.tarifaPlataforma.nombre,
      },
      tipo_tarifa_plataforma: {
        id: idTipoTarPlataforma,
        nombre: this.valTarifa.nombre,
        descripcion: this.valTarifa.descripcion,
        rango: rango,
      },
      tipo_tarifa_plataforma_id: idTipoTarPlataforma,
    };

    this.configuraciones.push( newConfig );

  }

  addTipoTarifa() {

    this.disaAdd = true;

    if (this.tiposTarifas.length > 0) {
      this.valTarifa = this.tiposTarifas.find( x => x.codigo == this.formularioTipoTarifa.value.tipoTarifa );
      this.tiposTarifasTabla.push( this.valTarifa );

      let indexTipoT = this.tiposTarifas.map(function(e) { return e.codigo; }).indexOf(this.valTarifa.codigo);

      this.tiposTarifas.splice(indexTipoT, 1);

      this.addConfiguracion( false, this.formularioTipoTarifa.value );
      this.getCardsConfiguraciones( this.configuraciones );

      $('#hasta, #desde').val('').attr('disabled', true);

      this.porcentaje = "false";
    }

    this.enableSave();

  }

  elimTipoTarifaCombo() {

    let a: number = 1;
    let b: number = 2;

    this.configuraciones.forEach(elemento => {

      if ( !elemento.tipo_tarifa_plataforma.rango ) {

        this.valTarifa = this.tiposTarifas.find( x => x.codigo == elemento.tipo_tarifa_plataforma.id);
        this.tiposTarifasTabla.push( this.valTarifa );

        let indexTipoT = this.tiposTarifas.map(function(e) { return e.codigo; }).indexOf(elemento.tipo_tarifa_plataforma.id);
        this.tiposTarifas.splice(indexTipoT, 1);

      } else {

        if ( a != b) {

          this.valTarifa = this.tiposTarifas.find( x => x.codigo == elemento.tipo_tarifa_plataforma.id);
          this.tiposTarifasTabla.push( this.valTarifa );

          let indexTipoT = this.tiposTarifas.map(function(e) { return e.codigo; }).indexOf(elemento.tipo_tarifa_plataforma.id);
          this.tiposTarifas.splice(indexTipoT, 1);

          a = b;
        }
      }
    });
  }

  getRango(event) {

    this.valTarifa = this.tiposTarifas.find( x => x.codigo == event.value );
    this.disaAdd = true;
    let disabletInput = true;
    let disabletHastaInput = true;

    if (this.valTarifa) {

      if (this.valTarifa.codigo == 1) {
        disabletInput = true;
        disabletHastaInput = true;
        this.disaAdd = false;
        this.porcentaje = "false";
      }

      if (this.valTarifa.codigo == 3) {
        disabletInput = false;
        disabletHastaInput = true;
        this.porcentaje = "success";
      }


      if (this.valTarifa.rango == true) {

        this.idRangoTarifa = this.valTarifa.codigo;
        this.showModalRango( this.idRangoTarifa );
        this.porcentaje = "false";
      }

      this.desdeValue = 0;
      this.hastaValue = '';
      this.formularioTipoTarifa.value.desde = '0';
      this.formularioTipoTarifa.value.hasta = '';

      $('#desde').val(0);
      $('#hasta').val('');
      $('#desde').attr('disabled', disabletInput);
      $('#hasta').attr('disabled', disabletHastaInput);
    }

  }

  validaDesde(event) {

    this.disaAdd = !event.target.value ? true : false;
  }

  deleteConfiguracion( tipoTarifaId: number, rango: any ) {

    if ( confirm("Seguro que desea eliminar?") ) {

      if (this.configuraciones.length > 0) {

        this.valTarifa = this.tiposTarifasTabla.find( x => x.codigo == tipoTarifaId);
        this.tiposTarifas.push( this.valTarifa );

        this.sortTipoTarifa();

        if ( !rango ) {
          let indexTipoT = this.configuraciones.map(function(e) { return e.tipo_tarifa_plataforma_id; }).indexOf(tipoTarifaId);
          this.configuraciones.splice(indexTipoT, 1);

        } else {

          let tipoT: any = this.configuraciones.filter(function(e) {
            return e.tipo_tarifa_plataforma_id == tipoTarifaId;
          });

          tipoT.forEach(elemento => {
            let indexTipoT = this.configuraciones.map(function(e) { return e.id; }).indexOf(elemento.id);
            this.configuraciones.splice(indexTipoT, 1);
          });

        }

        let indexTipoTCards = this.cardConfiguraciones.map(function(e) { return e.tipo_tarifa_plataforma_id; }).indexOf(tipoTarifaId);
        this.cardConfiguraciones.splice(indexTipoTCards, 1);
      }

      this.enableSave();
      this.closeModal();

    }

  }

  enableSave() {
    this.disaSave = (this.configuraciones.length <= 0) ? null : true;
    this.validaciones();
  }


  sortTipoTarifa() {

    this.tiposTarifas.sort(function(a, b) {
        return a.codigo - b.codigo;
    });

}

  save() {

    if ( this.tarifaPlataforma.codigo == 0 ) {

      this.store();

    } else if ( this.tarifaPlataforma.codigo > 0 ) {

      this.update();

    }

  }

  arrayConfiguracion() {

    let array: any = [];

    this.configuraciones.forEach(elemento => {
      let data: any = {
        codigo_tipo_tarifa: elemento.tipo_tarifa_plataforma.id,
        desde: elemento.desde,
        hasta: elemento.hasta,
        show_titulo: elemento.show_titulo,
        show_icon: elemento.show_icon
      };
      array.push( data );
    });

    return array;

  }

  validaciones() {

    this.formulario = new FormGroup({
      'nombre': new FormControl(this.tarifaPlataforma.nombre, [Validators.required]),
      'descripcion': new FormControl(this.tarifaPlataforma.descripcion, [Validators.required]),
      'vigencia': new FormControl(this.tarifaPlataforma.vigencia),
      'countRango': new FormControl(this.disaSave, [Validators.required]),
      'aplica': new FormControl(this.tarifaPlataforma.aplica, [Validators.required]),
    });

    let messages = new TarifasPlataformaMessages();
    this.messages = messages.getMessages();
    this.checkboxes();
  }

  checkboxes() {
    if ( this.tarifaPlataforma.aplica.length > 0 ) {
      let checkeds = this.tarifaPlataforma.aplica.split(";");
      checkeds.forEach(idChk => {
        let dato: number = parseFloat(idChk);
        let existe = this.data.entities.find( x => x.id == dato );
        existe.checked = 'true';
      });
    }
  }

  store() {

    let data: any = {
      nombre: this.formulario.value.nombre,
      descripcion: this.formulario.value.descripcion,
      aplica: this.formulario.value.aplica,
      vigencia: this.formulario.value.vigencia
    };

    this._loadingService.loadingIn();

    this._requestAuth.post( this.url + 'tarifaplataforma', data ).subscribe( (resp: any) => {

      this._loadingService.laodingOut();

      this.tarifaPlataforma = this._toastService.parse( resp );

      this._redirectService.editarTarifaPlataforma( this.tarifaPlataforma.codigo );

    }, (error) => {

      this._loadingService.laodingOut();

      this._toastService.errores( error );

    });

  }

  update() {

    let data: any = {
      nombre_tarifa: this.formulario.value.nombre,
      descripcion_tarifa: this.formulario.value.descripcion,
      aplica: this.formulario.value.aplica,
      vigencia_tarifa: this.formulario.value.vigencia,
      valores: this.arrayConfiguracion()
    };

    this._loadingService.loadingIn();

    let url: string = this.url + 'tarifaplataforma/' + this.tarifaPlataforma.codigo + '/configuraciontarifa';

    this._requestAuth.post( url, data ).subscribe( (resp: any) => {

      this._loadingService.laodingOut();

      this._toastService.showSuccess('¡Éxito!', 'Datos guardados con éxito');

    }, (error) => {

      this._loadingService.laodingOut();

      this._toastService.errores( error );

    });

  }

}
