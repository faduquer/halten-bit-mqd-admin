import { RouterModule, Routes } from '@angular/router';

// Components
import { TarifasPlataformaComponent } from './tarifas-plataforma.component';

// Guards
import {
    VerificaTokenGuard
} from '../../guards/guards.index';

const tarifasPlataformaRoutes: Routes = [
    {
        path: '',
        component: TarifasPlataformaComponent,
        canActivate: [
            VerificaTokenGuard
        ]
    },
    {
        path: ':id',
        loadChildren: './tarifa-plataforma/tarifa-plataforma.module#TarifaPlataformaModule',
        canActivate: [ VerificaTokenGuard ]
    },
];

export const TARIFAS_PLATAFORMA_ROUTES = RouterModule.forChild( tarifasPlataformaRoutes );
