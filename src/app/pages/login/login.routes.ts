import { RouterModule, Routes } from '@angular/router';

// Components
import { LoginComponent } from './login.component';

// Guards
import {
    VerificaTokenGuard,
    LoggedGuard
} from '../../guards/guards.index';

const loginRoutes: Routes = [
    {
        path: '',
        component: LoginComponent,
        canActivate: [
            VerificaTokenGuard,
            LoggedGuard
        ]
    }
];

export const LOGIN_ROUTES = RouterModule.forChild( loginRoutes );
