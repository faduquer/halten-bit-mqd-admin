import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

// Services
import {
  UsuarioService,
  RequestAuth,
  LoadingService,
  ToastService
} from '../../services/services.index';

import { URL_SERVICIOS } from '../../config/config';

// Models
import { Login } from '../../models/models.index';

declare function init_plugins();
declare var swal;
declare var $;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  url: string = URL_SERVICIOS;

  formulario: FormGroup;

  constructor(
    public router: Router,
    public _usuarioService: UsuarioService,
    private _requestAuth: RequestAuth,
    private _loadingService: LoadingService,
    private _toastService: ToastService
  ) { }

  ngOnInit() {

    init_plugins();

    this.validaciones();

  }

  validaciones() {
    this.formulario = new FormGroup({
      'email': new FormControl(''),
      'password': new FormControl('')
    });
  }

  ingresar() {

    /*
      DESARROLLO:
      client_id: 1,
      grant_type: 'password',
      client_secret: 'TFNMbTAFqFRfD9QVg1pm0d41iL4GYr1Hlachhwnk'
    */

    /*
      DILAN:
      client_id: 1,
      grant_type: 'password',
      client_secret: 'TFNMbTAFqFRfD9QVg1pm0d41iL4GYr1Hlachhwnk'
    */

    let login = new Login(
      this.formulario.value.email.toLowerCase(),
      this.formulario.value.password,
      1,
      1,
      'password',
      'TFNMbTAFqFRfD9QVg1pm0d41iL4GYr1Hlachhwnk'
    );

    this._loadingService.loadingIn();

    this._requestAuth.postNoToken( this.url + 'oauth/token', login).subscribe( (resp: any) => {

      this._loadingService.laodingOut();

      let data = JSON.parse( resp._body );
      this._usuarioService.guardarStorage( data );

      this.router.navigate(['/inicio']);

    }, (error) => {

      this._loadingService.laodingOut();

      this._toastService.errores( error );

    });

  }

}
