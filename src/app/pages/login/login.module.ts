import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LOGIN_ROUTES } from './login.routes';
import { LoginComponent } from './login.component';

@NgModule({
    declarations: [
        LoginComponent
    ],
    exports: [
        LoginComponent
    ],
    imports: [
        LOGIN_ROUTES,
        FormsModule,
        ReactiveFormsModule,
        CommonModule
    ]
})

export class LoginModule { }
