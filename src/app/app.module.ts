import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

// Routes
import { APP_ROUTES } from './app.routes';

// Components
import { AppComponent } from './app.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { PagesComponent } from './pages/pages.component';

// Modules
import { SharedModule } from './shared/shared.module';
import { ServicesModule } from './services/services.module';
import { GuardsModule } from './guards/guards.module';

@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent,
    PagesComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    APP_ROUTES,
    SharedModule,
    ServicesModule,
    GuardsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
