// PRODUCCION: http://18.216.243.224:8080/

// DESARROLLO: 10.0.32.51 apimqd -> http://10.0.32.51:8080/

// Dilan: http://10.0.33.182:80/mqd-api/public/

// Michael: http://mqdapi.com/

export const URL_SERVICIOS = 'http://localhost/mqd/Halten-bit-MQD-api/public/';

export const DataTableEspañol = {
    'emptyTable': 'Ningún dato disponible en esta tabla',
    'info': 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
    'infoEmpty': 'Mostrando registros del 0 al 0 de un total de 0 registros',
    'infoFiltered': '(filtrado de un total de _MAX_ registros)',
    'infoPostFix': '',
    'infoThousands': ',',
    'loadingRecords': 'Cargando...',
    'lengthMenu': 'Mostrar _MENU_ registros',
    'processing': 'Procesando...',
    'search': 'Buscar:',
    'url': '',
    'zeroRecords': 'No se encontraron resultados',
    'paginate': {
        'first': '&laquo;&laquo;',
        'previous': '&laquo;',
        'next': '&raquo;',
        'last': '&raquo;&raquo;'
    },
    'aria': {
        'sortAscending': ': Activar para ordenar la columna de manera ascendente',
        'sortDescending': ': Activar para ordenar la columna de manera descendente'
    }
};
