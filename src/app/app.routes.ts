import { RouterModule, Routes } from '@angular/router';

// Guards
import {
    VerificaTokenGuard,
    LoggedGuard,
    LoginGuard
} from './guards/guards.index';

// Components
import { PagesComponent } from './pages/pages.component';

const appRoutes: Routes = [
    {
        path: 'login',
        loadChildren: './pages/login/login.module#LoginModule',
        canActivate: [
            VerificaTokenGuard,
            LoggedGuard
        ]
    },
    {
        path: 'access-denied',
        loadChildren: './errors/access-denied/access-denied.module#AccessDeniedModule',
        canActivate: [ VerificaTokenGuard ]
    },
    {
        path: 'no-page-found',
        loadChildren: './errors/no-page-found/no-page-found.module#NoPageFoundModule',
        canActivate: [ VerificaTokenGuard ]
    },
    {
        path: '',
        component: PagesComponent,
        canActivate: [
            LoginGuard,
            VerificaTokenGuard
        ],
        loadChildren: './pages/pages.module#PagesModule'
    },
    {
        path: '**',
        loadChildren: './errors/no-page-found/no-page-found.module#NoPageFoundModule',
        canActivate: [ VerificaTokenGuard ]
    }
];

export const APP_ROUTES = RouterModule.forRoot( appRoutes );
