import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routes
import { ACCESS_DENIED_ROUTES } from './access-denied.routes';

// Components
import { AccessDeniedComponent } from './access-denied.component';

@NgModule({
    declarations: [
        AccessDeniedComponent
    ],
    exports: [
        AccessDeniedComponent
    ],
    imports: [
        ACCESS_DENIED_ROUTES,
        CommonModule
    ]
})

export class AccessDeniedModule { }
