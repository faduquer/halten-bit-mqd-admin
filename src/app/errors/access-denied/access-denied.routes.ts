import { RouterModule, Routes } from '@angular/router';

// Component
import { AccessDeniedComponent } from './access-denied.component';

// Guard
// import { VerificaTokenGuard } from '../../services/service.index';

const accessDeniedRoutes: Routes = [
    {
        path: '',
        component: AccessDeniedComponent,
        // canActivate: [ VerificaTokenGuard ]
    }
];

export const ACCESS_DENIED_ROUTES = RouterModule.forChild( accessDeniedRoutes );
