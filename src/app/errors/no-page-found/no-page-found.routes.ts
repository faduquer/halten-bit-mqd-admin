import { RouterModule, Routes } from '@angular/router';

// Component
import { NopagefoundComponent } from './no-page-found.component';

// Guard
// import { VerificaTokenGuard } from '../../services/service.index';

const noPageFoundRoutes: Routes = [
    {
        path: '',
        component: NopagefoundComponent,
        // canActivate: [ VerificaTokenGuard ]
    }
];

export const NO_PAGE_FOUND_ROUTES = RouterModule.forChild( noPageFoundRoutes );
