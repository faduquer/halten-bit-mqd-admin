import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nopagefound',
  templateUrl: './no-page-found.component.html',
  styleUrls: ['./no-page-found.component.css']
})
export class NopagefoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
