import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routes
import { NO_PAGE_FOUND_ROUTES } from './no-page-found.routes';

// Components
import { NopagefoundComponent } from './no-page-found.component';

@NgModule({
    declarations: [
        NopagefoundComponent
    ],
    exports: [
        NopagefoundComponent
    ],
    imports: [
        NO_PAGE_FOUND_ROUTES,
        CommonModule
    ]
})

export class NoPageFoundModule { }
