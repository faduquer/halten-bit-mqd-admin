export class Usuario {

    constructor (
        public id: number,
        public name: string,
        public seudonimo: string,
        public username: string,
        public codigo: number,
        public tipo: number,
        public tipoNombre: string,
        public temas: boolean,
    ) { }

}
