export class Login {

    constructor (
        public username: string,
        public password: string,
        public tipo: number,
        public client_id: number,
        public grant_type: string,
        public client_secret: string,
    ) { }

}
