export interface TarifasPlataforma {
    codigo: number;
    descripcion: string;
    fecha_actualizacion: string;
    fecha_creacion: string;
    fecha_eliminacion: string;
    nombre: string;
    vigencia: boolean;
    aplica: string;
}
