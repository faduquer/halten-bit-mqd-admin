export interface TiposTarifas {
    codigo: number;
    descripcion: string;
    fecha_actualizacion: string;
    fecha_creacion: string;
    fecha_eliminacion: string;
    nombre: string;
    vigencia: boolean;
    admin: boolean;
    rango: boolean;
    hidden: boolean;
}
