export interface ConfiguracionTarifas {
    adicional: boolean;
    excluyente: boolean;
    expmaxima: string;
    expminima: string;
    tarifas: any;
    tipo_tarifa: string;
    valmaximo: string;
    valminimo: string;
    valores: any;
}
