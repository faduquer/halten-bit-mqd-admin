export interface AtributosConfigurar {
    atributo_valor: any;
    cols: number;
    formato_id: number;
    id: number;
    nb_atributo: string;
    pagina: number;
    required: boolean;
    tipo_campo_id: number;
    tipocampo: any;
    validacion: any;
    validacion_id: number;
}
