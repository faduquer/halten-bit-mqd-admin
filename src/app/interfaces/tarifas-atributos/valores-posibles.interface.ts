export interface ValoresPosibles {
    atributo_id: number;
    created_at: string;
    deleted_at: string;
    fe_vigencia: string;
    formato_id: number;
    id: number;
    updated_at: string;
    vigencia: boolean;
    vl_atributo_valor: string;
}
