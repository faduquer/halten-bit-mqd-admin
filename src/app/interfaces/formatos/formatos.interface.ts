export interface Formatos {
    acronimo: string;
    codigo: number;
    descripcion: string;
    estatus: number;
    fecha_creacion: string;
    fecha_actualizacion: string;
    fecha_eliminacion: string;
    nombre: string;
}
