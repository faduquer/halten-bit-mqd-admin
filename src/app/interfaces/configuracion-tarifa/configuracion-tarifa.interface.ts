export interface ConfiguracionTarifa {
    aplica_desde: string;
    aplica_hasta: string;
    created_at: string;
    deleted_at: string;
    desde: string;
    hasta: string;
    show_titulo: boolean;
    show_icon: boolean;
    rango: boolean;
    rowspan: any;
    id: number;
    tarifa: string;
    tarifa_plataforma: any;
    tarifa_plataforma_id: number;
    tipo_tarifa_plataforma: any;
    tipo_tarifa_plataforma_id: number;
    updated_at: string;
    vigencia: boolean;
    conf_tarifa_formato_id: number;
    activo: any;
}
