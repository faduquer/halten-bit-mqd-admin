export interface TarifasConfiguradas {
    created_at: string;
    deleted_at: string;
    descripcion: string;
    id: number;
    nombre: string;
    updated_at: string;
    vigencia: boolean;
}
