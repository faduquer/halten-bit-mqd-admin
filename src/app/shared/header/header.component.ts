import { Component, OnInit } from '@angular/core';

// Services
import { UsuarioService } from '../../services/services.index';

// Models
import { Usuario } from '../../models/models.index';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  usuario: Usuario;

  constructor(
    public _usuarioService: UsuarioService,
  ) { }

  ngOnInit() {

    this.usuario = this._usuarioService.usuario;

  }

}
