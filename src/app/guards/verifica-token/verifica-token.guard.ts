import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

// Config
import { URL_SERVICIOS } from '../../config/config';

// Services
import {
    RequestAuth,
    UsuarioService
} from '../../services/services.index';

@Injectable()
export class VerificaTokenGuard implements CanActivate {

  log: boolean = false;

  url: string = URL_SERVICIOS;

  contador: number = 0;

  constructor(
    private _requestAuth: RequestAuth,
    public _usuarioService: UsuarioService
  ) {

    this.log = this._usuarioService.estaLogueado();

  }

  canActivate(): Promise<boolean> | boolean {

    let token = this._usuarioService.token;

    if ( token === '' ) {

      this.obtenerTokenClient();
      return true;

    } else {

      let payload = JSON.parse( atob( token.split('.')[1] ) );

      let expirado = this.expirado( payload.exp );

      if ( expirado ) {

        if ( this.log ) {

          this._usuarioService.logout();
          return true;

        } else {

          this.obtenerTokenClient();
          return true;

        }

      }

      return this.verificaRenueva( payload.exp );

    }

  }

  verificaRenueva( fechaExp: number ): Promise<boolean> {

    return new Promise( ( resolve, reject ) => {

      let tokenExp = new Date( fechaExp * 1000 );
      let ahora = new Date();

      let diff = ( tokenExp.getTime() - ahora.getTime()) / 1000;
      diff /= 60;
      diff = Math.abs(Math.round(diff));

      if ( this.log ) {

        if ( diff < 60 ) {

          this._requestAuth.refreshToken().subscribe( (resp: any) => {

            let tokens: any = JSON.parse( resp._body );

            this._usuarioService.guardarTokensStorage( tokens );

          });

        }

      } else {

        if ( diff < 60 ) {

          this.obtenerTokenClient();

        }

      }

      resolve( true );

    });

  }

  expirado( fechaExp: number ) {

    let ahora = new Date().getTime() / 1000;

    if ( fechaExp < ahora ) {

      return true;

    } else {

      return false;

    }

  }

  obtenerTokenClient() {

    this._requestAuth.getToken().subscribe( (resp: any) => {

      this._requestAuth.guardarTokenClient( resp );

    }, error => {

      if ( this.contador < 5 ) {

        this.obtenerTokenClient();
        this.contador++;

      }

    });

  }

}
