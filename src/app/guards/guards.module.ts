import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

// Guards
import {
    LoggedGuard,
    LoginGuard,
    VerificaTokenGuard
} from './guards.index';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    LoggedGuard,
    LoginGuard,
    VerificaTokenGuard
  ],
  declarations: []
})
export class GuardsModule { }
