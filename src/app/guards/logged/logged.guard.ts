import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

// Services
import { UsuarioService } from '../../services/services.index';

@Injectable()
export class LoggedGuard implements CanActivate {

  constructor(
    public _usuarioService: UsuarioService,
    public _router: Router
  ) {}

  canActivate() {

    if ( !this._usuarioService.estaLogueado() ) {

      return true;

    } else {

      this._router.navigate(['inicio']);
      return false;

    }

  }

}
