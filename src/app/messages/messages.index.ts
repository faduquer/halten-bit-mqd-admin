// Tarifas Atributos
export * from './tarifas-atributos/tarifas-atributos.message';

// Tarifas Formatos
export * from './tarifas-formatos/tarifas-formatos.message';

// Tarifas Plataforma
export * from './tarifas-plataforma/tarifas-plataforma.message';
