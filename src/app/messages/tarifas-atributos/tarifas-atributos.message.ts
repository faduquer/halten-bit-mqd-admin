export class TarifasAtributosMessages {

    messages = {
        'expresion_min': [
            { type: 'required', message: 'Campo requerido.' }
        ],
        'expresion_max': [
            { type: 'required', message: 'Campo requerido.' }
        ],
        'valor_min': [
            { type: 'required', message: 'Campo requerido.' }
        ],
        'valor_max': [
            { type: 'required', message: 'Campo requerido.' }
        ],
        'codigo_formato': [
            { type: 'required', message: 'Campo requerido.' }
        ],
        'atributo_id': [
            { type: 'required', message: 'Campo requerido.' }
        ],
    };

    getMessages() {

        return this.messages;

    }

}
