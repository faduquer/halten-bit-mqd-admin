export class TarifasPlataformaMessages {

    messages = {
        'nombre': [
            { type: 'required', message: 'Campo requerido.' }
        ],
        'descripcion': [
            { type: 'required', message: 'Campo requerido.' }
        ]
    };

    getMessages() {

        return this.messages;

    }

}
