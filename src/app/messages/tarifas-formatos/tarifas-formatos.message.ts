export class TarifasFormatosMessages {

    messages = {
        'formato': [
            { type: 'required', message: 'Campo requerido.' }
        ]
    };

    getMessages() {

        return this.messages;

    }

    setValidators( configuraciones: any ) {

        return this.setValidatorMessage( configuraciones );

    }

    setValidatorMessage( configuraciones: any ) {

        for ( let i = 0; i < configuraciones.length; i++ ) {

            this.messages[ "tarifa" + i ] = [
                { type: 'required', message: 'Campo requerido.' }
            ];

            this.messages[ "desde" + i ] = [
               { type: 'required', message: 'Campo requerido.' },
               { type: 'pattern', message: 'Formato inválido.' }
            ];

            this.messages[ "hasta" + i ] = [
                { type: 'required', message: 'Campo requerido.' },
                { type: 'pattern', message: 'Formato inválido.' }
             ];

        }

        return this.messages;

    }

}
