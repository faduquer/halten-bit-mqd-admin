import { Injectable } from '@angular/core';

declare var $;

@Injectable()
export class LoadingService {

  constructor() { }

  loadingIn() {

    $('.overlay').fadeIn().css('display', 'table');

  }

  laodingOut() {

    $('.overlay').fadeOut().css('display', 'none');

  }

}
