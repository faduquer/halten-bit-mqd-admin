import { Injectable } from '@angular/core';
import { RequestOptions , Headers, Http } from '@angular/http';
import 'rxjs/add/operator/map';

// Config
import { URL_SERVICIOS } from '../config/config';

// Models
import { RefreshToken } from '../models/models.index';

@Injectable()
export class RequestAuth {

  url: string = URL_SERVICIOS;
  grant_type: any = 'client_credentials';
  client_id: number = 2;
  client_secret: any = 'h8S0a1Mn1pOzmEUA94gjt691erWJYn04NvufjVp2';


  constructor(
    private http: Http
  ) {}

  obtenerTokenLocalStorage() {

    return localStorage.getItem('token');

  }

  getToken() {

    let url = 'oauth/token';
    let data = {
      grant_type: this.grant_type,
      client_id: this.client_id,
      client_secret: this.client_secret
    };

    return this.http.post( this.url + url, data ).map( (resp) => {
        return resp.json();
    });

  }

  get( url, auth = true ) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    if ( auth ) {
      let tok = "Bearer " + this.obtenerTokenLocalStorage();
      headers.append( 'Authorization', tok );
    }

    let options = new RequestOptions({ headers : headers });

    return this.http.get( url, options ).map((resp) => {
      return resp.json();
    });

  }

  post( url: string, data: any, auth = true) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    if ( auth ) {
      let tok = "Bearer " + this.obtenerTokenLocalStorage();
      headers.append( 'Authorization', tok );
    }


    let options = new RequestOptions({ headers : headers });

    return this.http.post( url, JSON.stringify( data ), options ).map( (resp) => {
        return resp;
    });


  }

  put( url: string, data: any, auth = true) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    if ( auth ) {
      let tok = "Bearer " + this.obtenerTokenLocalStorage();
      headers.append( 'Authorization', tok );
    }

    let options = new RequestOptions({ headers : headers });

    return this.http.put( url, JSON.stringify( data ), options ).map( (resp) => {
        return resp;
    });


  }

  delete( url: string, auth = true) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    if ( auth ) {
      let tok = "Bearer " + this.obtenerTokenLocalStorage();
      headers.append( 'Authorization', tok );
    }

    let options = new RequestOptions({ headers : headers });

    return this.http.delete( url, options ).map( (resp) => {
        return resp;
    });


  }

  getNoToken( url ) {

    let headers = new Headers();
    headers.append( 'Content-Type', 'application/json' );

    let options = new RequestOptions({ headers : headers });

    return this.http.get( url, options ).map( (data) => {
      return data.json();
    });

  }

  postNoToken( url: string, data: any ) {

    let headers = new Headers();
    headers.append( 'Content-Type', 'application/json' );

    let options = new RequestOptions({ headers : headers });

    return this.http.post( url, JSON.stringify( data ), options).map( (resp) => {
      return resp;
    });

  }

  postTokenRecuperar( url: string, data: any, token: any, auth = true) {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    if ( auth ) {
      let tok = "Bearer " + token;
      headers.append( 'Authorization', tok );
    }


    let options = new RequestOptions({ headers : headers });

    return this.http.post( url, JSON.stringify( data ), options );


  }

  getJsonData( url ) {

    // console.log(url);

    return this.http.get( url ).map( (data) => {
      return data.json();
    });

  }

  guardarTokenClient( data ) {
    localStorage.setItem('token', data.access_token);
  }

  refreshToken() {

      let data = new RefreshToken (
          'refresh_token',
          1,
          'TFNMbTAFqFRfD9QVg1pm0d41iL4GYr1Hlachhwnk',
          localStorage.getItem('refresh_token')
      );

      return this.http.post( this.url + 'oauth/token', data ).map( (resp) => {
          return resp;
      });

  }


}
