import { Injectable } from '@angular/core';

// Config
import { URL_SERVICIOS } from '../../config/config';

// Services
import { RequestAuth } from '../request-auth.service';

@Injectable()
export class ConfiguracionTarifaService {

  url: string = URL_SERVICIOS;

  constructor(
    private _requestAuth: RequestAuth
  ) { }

  getConfiguraciones( tarifa: number ) {

    return this._requestAuth.get( this.url + 'tarifaplataforma/' + tarifa + '/configuraciontarifa' ).map( (data) => {
      return data;
    });

  }

}
