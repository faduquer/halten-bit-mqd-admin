import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Injectable()
export class ToastService {

  constructor(
    private toastr: ToastrService,
    private _router: Router
  ) { }

  showSuccess( title: string, message: string ) {

    this.toastr.success( message, title, {
      timeOut: 4000,
      closeButton: true
    });

  }

  showError( title: string, message: string ) {

    this.toastr.error( message, title, {
      timeOut: 4000,
      closeButton: true
    });

  }

  showWarning( title: string, message: string ) {

    this.toastr.warning( message, title, {
      timeOut: 4000,
      closeButton: true
    });

  }

  errorsLists( errors: any ) {

    let keys: any = Object.keys( errors );

    for ( let i = 0; i < keys.length; i++ ) {

      let messages = errors[keys[i]];

      for ( let x = 0; x < messages.length; x++ ) {

        this.showError( keys[i], messages[x] );

      }

    }

  }

  errores( error: any ) {

    if ( error.json().code === 401 ) {

      this.showError( '¡Error!', '¡Se ha presentado un error, por favor actualice la página!' );

    } else if ( error.json().code === 403 ) {

      this._router.navigate(['/access-denied']);

    } else if ( error.json().code === 404 ) {

      this._router.navigate(['/no-page-found']);

    } else if ( error.json().code === 409 ) {

      this.showWarning( '¡Aviso!', '¡' + error.json().error + '!' );

    } else if ( error.json().code === 422 ) {

      this.errorsLists( error.json().error );

    } else if ( error.json().code === 500 ) {

      // this._redirectService.internalError();

      this.showError( '¡Error!', '¡Se ha presentado un incoveniente, por favor intentelo de nuevo!' );

    } else {

      this.showError( '¡Error!', '¡Se ha presentado un incoveniente, por favor intentelo de nuevo!' );

    }

  }

  parse( data: any ) {

    let resp = JSON.parse( data._body );

    return resp.data;

  }


}
