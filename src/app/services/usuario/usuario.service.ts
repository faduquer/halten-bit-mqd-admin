import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

// Models
import { Usuario } from '../../models/models.index';

// Services
import { RequestAuth } from '../request-auth.service';

@Injectable()
export class UsuarioService {

  usuario: Usuario;
  token: string;
  refresh_token: string;

  constructor(
    public http: Http,
    public router: Router,
    private _requestAuth: RequestAuth
  ) {

    this.cargarStorage();

  }

  estaLogueado() {
    return ( this.token.length > 5 && this.usuario ) ? true : false;
  }

  cargarStorage() {

    if ( localStorage.getItem('token') ) {
      this.token = localStorage.getItem('token');
      this.refresh_token = localStorage.getItem('refresh_token');
      this.usuario = JSON.parse( localStorage.getItem('usuario') );
    } else {
      this.token = '';
      this.refresh_token = '';
      this.usuario = null;
    }

  }

  guardarStorage( data ) {

    let tipoNombre = '';

    if ( data.tipo == 3 ) {
      tipoNombre = 'administrador';
    }

    let user = new Usuario(
      data.id,
      data.name,
      data.seudonimo,
      data.username,
      data.codigo,
      data.tipo,
      tipoNombre,
      data.temas
    );

    localStorage.setItem('token', data.access_token);
    localStorage.setItem('refresh_token', data.refresh_token);
    localStorage.setItem('usuario', JSON.stringify(user) );

    this.usuario = user;
    this.token = data.access_token;

  }

  logout() {

    this.usuario = null;
    this.token = '';
    this.refresh_token = '';

    localStorage.removeItem('token');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('usuario');

    this.router.navigate(['/login']);

  }

  guardarTokensStorage( data ) {

    localStorage.setItem('token', data.access_token);
    localStorage.setItem('refresh_token', data.refresh_token );

    this.token = data.access_token;
    this.refresh_token = data.refresh_token;

  }

}
