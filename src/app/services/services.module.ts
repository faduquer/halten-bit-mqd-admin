import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import {
  ConfiguracionTarifaService,
  FormatosService,
  LoadingService,
  RedirectService,
  RequestAuth,
  SidebarService,
  TarifasAtributosService,
  TarifasFormatosService,
  TarifasPlataformaService,
  TiposTarifasService,
  ToastService,
  UsuarioService
} from './services.index';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    ConfiguracionTarifaService,
    FormatosService,
    LoadingService,
    RedirectService,
    RequestAuth,
    SidebarService,
    TarifasAtributosService,
    TarifasFormatosService,
    TarifasPlataformaService,
    TiposTarifasService,
    ToastService,
    UsuarioService
  ],
  declarations: []
})
export class ServicesModule { }
