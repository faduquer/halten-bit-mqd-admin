import { Injectable } from '@angular/core';

// Config
import { URL_SERVICIOS } from '../../config/config';

// Services
import { RequestAuth } from '../request-auth.service';

@Injectable()
export class TiposTarifasService {

  url: string = URL_SERVICIOS;

  constructor(
    private _requestAuth: RequestAuth
  ) { }

  getTiposTarifas() {

    let url: string = this.url + 'tipotarifa';

    return this._requestAuth.get( url ).map( (data) => {
      return data;
    });

  }

}
