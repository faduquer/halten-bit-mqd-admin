import { Injectable } from '@angular/core';

// Config
import { URL_SERVICIOS } from '../../config/config';

// Services
import { RequestAuth } from '../request-auth.service';

@Injectable()
export class TarifasFormatosService {

  url: string = URL_SERVICIOS;

  constructor(
    private _requestAuth: RequestAuth
  ) { }

  getTarifasConfiguradas() {

    let url: string = this.url + 'tarifaconfigurada';

    return this._requestAuth.get( url ).map( (data) => {
      return data;
    });

  }

  getFormatoConfiguraciones( formato: number ) {

    let url: string = this.url + 'formato/' + formato + '/configuraciontarifaformato';

    return this._requestAuth.get( url ).map( (data) => {
      return data;
    });

  }

}
