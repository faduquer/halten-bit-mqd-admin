import { Injectable } from '@angular/core';

@Injectable()
export class SidebarService {

  menu: any = [
    {
      titulo: 'Principal',
      icono: 'mdi mdi-gauge',
      submenu: [
        { titulo: 'Inicio' , url: '/inicio' }
      ]
    },
    {
      titulo: 'Tarifas',
      icono: 'mdi mdi-clipboard-outline',
      submenu: [
        { titulo: 'Tarifas por atributos' , url: '/tarifas-atributos' },
        { titulo: 'Tarifas plataforma' , url: '/tarifas-plataforma' },
        { titulo: 'Tarifas por formatos' , url: '/tarifas-formatos' }
      ]
    }
  ];

  constructor() { }

}
