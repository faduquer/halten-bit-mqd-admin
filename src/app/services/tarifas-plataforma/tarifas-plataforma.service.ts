import { Injectable } from '@angular/core';

// Config
import { URL_SERVICIOS } from '../../config/config';

// Services
import { RequestAuth } from '../request-auth.service';

@Injectable()
export class TarifasPlataformaService {

  url: string = URL_SERVICIOS;

  constructor(
    private _requestAuth: RequestAuth
  ) { }

  getTarifaPlataforma( id: number ) {

    return this._requestAuth.get( this.url + 'tarifaplataforma/' + id ).map( (data) => {
      return data;
    });

  }

}
