import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class RedirectService {

  constructor(
    private router: Router
  ) { }

  noPageFound() {
    this.router.navigate(['no-page-found']);
  }

  // -------------------------------------------- //
  // Tarifas Plataforma
  editarTarifaPlataforma( id ) {
    this.router.navigate(['tarifas-plataforma', id]);
  }
  // -------------------------------------------- //


}
