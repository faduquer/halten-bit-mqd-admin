import { Injectable } from '@angular/core';

// Services
import { RequestAuth } from '../request-auth.service';

// Config
import { URL_SERVICIOS } from '../../config/config';

@Injectable()
export class TarifasAtributosService {

  url: string = URL_SERVICIOS;

  constructor(
    private _requestAuth: RequestAuth
  ) { }

  getAtributosConfigurar( formatoTarifa: number ) {

    let url: string = this.url + 'formato/' + formatoTarifa + '/atributos';

    return this._requestAuth.get( url ).map( (data) => {
      return data;
    });

  }

  getValoresPosibles( formatoTarifa: number, atributoConfigurar: number ) {

    let url: string = this.url + 'formato/' + formatoTarifa + '/atributos/' + atributoConfigurar;

    return this._requestAuth.get( url ).map( (data) => {
      return data;
    });

  }

  getConfiguracionTarifa( formato: number, atributo: number ) {

    let url = this.url + 'configuraciontarifa/' + formato + '/' + atributo;

    return this._requestAuth.get( url ).map( (data) => {
      return data;
    });

  }

}
