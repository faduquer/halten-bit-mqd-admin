import { Injectable } from '@angular/core';

// Config
import { URL_SERVICIOS } from '../../config/config';

// Services
import { RequestAuth } from '../request-auth.service';

@Injectable()
export class FormatosService {

  url: string = URL_SERVICIOS;

  constructor(
    private _requestAuth: RequestAuth
  ) { }

  getFormatos( comun: boolean = false ) {

    let urlComun: string = "";

    if ( comun ) {
      urlComun = '?comun=' + comun;
    }

    return this._requestAuth.get( this.url + 'formatos' + urlComun ).map( (data) => {
      return data;
    });

  }

}
