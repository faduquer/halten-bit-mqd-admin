import { NgModule } from '@angular/core';
import { NombreUsuarioPipe } from './nombre-usuario.pipe';

@NgModule({
  imports: [],
  declarations: [
    NombreUsuarioPipe
  ],
  exports: [
    NombreUsuarioPipe
  ]
})
export class PipesModule { }
