import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nombreUsuario'
})
export class NombreUsuarioPipe implements PipeTransform {

  transform(value: string, todas:boolean = true): any {
    
    value = value.toLowerCase();
    if (value.length > 15) {
      value = value.substr(0,14) + "...";
    }
    let nombres = value.split(" ");
    if ( todas ) {
      // tslint:disable-next-line:forin
      for ( let i in nombres ) {
        if ( nombres[i] != "" ) {
          nombres[i] = nombres[i][0].toUpperCase() + nombres[i].substr(1);
        }
      }
    } else {
      nombres[0] = nombres[0][0].toUpperCase() + nombres[0].substr(1);
    }
    return nombres.join(" ");

  }

}
